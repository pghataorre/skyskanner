import React from 'react';
import Header from '../Header';
import FlightResults from '../flightResults/flightResults';

const App = () => (
  <div>
    <Header />
    <main>
      <FlightResults />
    </main>
  </div>
);

export default App;
