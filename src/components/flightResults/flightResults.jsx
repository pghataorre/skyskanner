import React, { Component } from 'react';
import ItinList from './itinList';

class FlightResults extends Component {
  constructor() {
    super();
    this.state = {
      flightJson: null
    };

    this.getData();
  }

  getData() {
    const url = `http://localhost:3333/get-flight-info`;
    let flightInfo;

    fetch(url)
      .then(response => response.json())
      .then((response) => {
        debugger;
        flightInfo = response;
      })
      .catch((err) => {
        flightInfo =  {error: 'error'};
      });

      debugger;

    this.setState({flightJson: flightInfo});
  }

  render() {
    const {itineraries, legs} = this.state.flightJson;

    {if (!itineraries) {
      return (
        <section>
          <ol>
            {itineraries.map((itinItem) => {
              return (<ItinList itinItem={itinItem} legsCollection={legs} />)
            })}
          </ol>
        </section>
      )} else {
      return (<h1>Sorry please refresh and try again</h1>)
      }
    }
  }
}

export default FlightResults;
