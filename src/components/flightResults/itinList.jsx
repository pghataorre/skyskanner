import React from 'react';
import LegsDetailsList from './legsDetailsList';

const ItinList = (props) => {
  const {id, price, agent, legs} = props.itinItem;
  const legCollection = props.legsCollection;
  return (
    <li className="itin-results" key={id}>
      <span className="legs-info">
        <LegsDetailsList legsItem={legs} legCollection={legCollection} />
      </span>
      <span>
        <span>
          {price}
        </span>
        <span>
          {agent}
        </span>
      </span>
      <span>
        <button id="{`select${index}`}">Select</button>
      </span>
    </li>
  )
}

export default ItinList;
