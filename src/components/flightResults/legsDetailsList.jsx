import React from 'react';
import Moment from 'react-moment';
import 'moment-timezone';

const LegsDetailsList = (props) => {
  const legIdItems = props.legsItem;
  const legCollection = props.legCollection;

  let legsInfoArr = legIdItems.length
    ? legIdItems.map((itemId) => {
      let tempArr = legCollection.filter((legsItem) => {
        return itemId === legsItem.id ? legsItem : null;
      });
      return tempArr[0];
      })
    : [];

  return (
  <ol className="legs-info">
    { legsInfoArr.length > 0 ? legsInfoArr.map((item, index) => (
      <li key={index}>
        <span className="logo">
          <img src={`https://logos.skyscnr.com/images/airlines/favicon/${item.airline_id}.png}`} width="25" height="25" />
        </span>
        <span className="departure-info">
          <span className="depart-time">{formatTime(item.departure_time)}</span>
          <span>{item.departure_airport}</span>
        </span>
        <span className="arrow">
          =>
        </span>
        <span className="arrival-info">
          <span className="arrival-time">{formatTime(item.arrival_time)}</span>
          <span>{item.arrival_airport}</span>
        </span>
        <span className="spacer">
        </span>
        <span className="flight-info">
          <span className="flight-time">{item.duration_mins}</span>
          <span>{isDirectFligt(item.stops)}</span>
        </span>
      </li>
    )) : null}
  </ol>)
}

const formatTime = (timeItem) => {
  return (<Moment format="HH:mm">{timeItem}</Moment>)
}

const isDirectFligt = (fligtStops) => {
  if (fligtStops === 0) {
    return'Direct'
  } else if (fligtStops === 1) {
    return `${fligtStops} stop`
  } else if (fligtStops > 1) {
    return `${fligtStops} stops`
  }
}

export default LegsDetailsList;
